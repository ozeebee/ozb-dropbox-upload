organization := "org.ozb"

name := "ozb-dropbox-upload"

version := "0.1"

scalaVersion := "2.11.8"

libraryDependencies ++= Seq(
	"com.dropbox.core" % "dropbox-core-sdk" % "2.1.1",
	"com.github.scopt" %% "scopt" % "3.5.0",
	"org.ozb" %% "ozb-scala-utils" % "0.2",
	// configuration
	"com.typesafe" % "config" % "1.3.0",
	// logging
	"org.slf4j" %  "slf4j-api" % "1.7.+",
	"ch.qos.logback" % "logback-classic" % "1.1.7",
	// testing
	"org.scalatest" %% "scalatest" % "2.2.1" % "test"
)

scalacOptions ++= Seq("-deprecation", "-unchecked", "-feature")

enablePlugins(JavaAppPackaging)

// see http://www.scala-sbt.org/sbt-native-packager/archetypes/java_app/customize.html
javaOptions in Universal ++= Seq(
    "-Dapple.awt.UIElement=true"
)
