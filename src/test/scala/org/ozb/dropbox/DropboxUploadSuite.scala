package org.ozb.dropbox

import org.scalatest.FunSuite

class DropboxUploadSuite extends FunSuite {
	test("Command line : keychain specified") {
		val cmdline = "-k somefile".split(" ")
		assert(DropboxUpload.parseCommandLine(cmdline).isDefined)
		assert(DropboxUpload.parseCommandLine(cmdline).get.useKeychain == true)
		assert(DropboxUpload.parseCommandLine(cmdline).get.useToken == false)
	}

	test("Command line : token specified") {
		val cmdline = "-t somefile".split(" ")
		assert(DropboxUpload.parseCommandLine(cmdline).isDefined)
		assert(DropboxUpload.parseCommandLine(cmdline).get.useKeychain == false)
		assert(DropboxUpload.parseCommandLine(cmdline).get.useToken == true)
	}
	
	test("Command line : no token or keychain specified") {
		val cmdline = "somefile".split(" ")
		assert(DropboxUpload.parseCommandLine(cmdline).isEmpty)
	}
	
	test("Command line : both token and keychain specified") {
		val cmdline = "-k -t somefile".split(" ")
		assert(DropboxUpload.parseCommandLine(cmdline).isEmpty)
	}

}