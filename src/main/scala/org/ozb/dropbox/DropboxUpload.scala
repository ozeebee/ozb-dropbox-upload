package org.ozb.dropbox

import org.ozb.utils.osx.KeychainUtils
import org.ozb.utils.io.IOUtils.withInputStream

import java.util.Locale
import java.io.File
import scopt.OptionParser
import com.dropbox.core.DbxAppInfo
import com.dropbox.core.DbxRequestConfig
import com.dropbox.core.v2.DbxClientV2
import com.dropbox.core.v2.files.WriteMode
import com.dropbox.core.DbxWebAuth
import com.dropbox.core.DbxAuthFinish

import org.slf4j.LoggerFactory

import com.typesafe.config.ConfigFactory

/* usage:
ozb-dropbox-upload 0.1 : upload file to dropbox
Usage: ozb-dropbox-upload [options] <file> [<destPath>]

  --help
        prints this usage text
  -k | --keychain
        use Mac OS X keychain to retrieve access token (name = 'ozb-dropbox-upload.accessToken')
  -t | --token
        use token for authorization, you have to visit the displayed url to retrieve it
  -o | --overwrite
        overwrite the file if it already exists at destination (by default the file is renamed)
  <file>
        file to upload
  <destPath>
        destination path: either a directory (terminated by a slash, e.g. '/Temp/') or a filename ('/Somedir/somefile.ext'). If not specified, this will be '/file'
*/
object DropboxUpload {
	lazy val log = LoggerFactory.getLogger(this.getClass)

	val toolName = "ozb-dropbox-upload"
	val toolVersion = "0.1"

	case class Config(
		fileToUpload: File = null,
		destPath: Option[String] = None,
		useKeychain: Boolean = false,
		useToken: Boolean = false,
		overwrite: Boolean = false,
		appinfoConfFile: String = System.getProperty("user.home") + "/.java/ozb-dropbox-upload.appinfo.conf"
	)

	val accessTokenKeychainServiceName = "ozb-dropbox-upload.accessToken"
		
	def main(args: Array[String]): Unit = {
		log.info("***** starting *****")
	
		val config = parseCommandLine(args).getOrElse(sys.exit(2))
		authorize(config.useKeychain, config.appinfoConfFile) match {
			case Right(client) => upload(client, config.fileToUpload, config.overwrite, config.destPath)
			case Left(errmsg) => 
				outln(errmsg)
				sys.exit(1)
		}
		log.info("***** finished *****")
	}
	
	def parseCommandLine(args: Array[String]): Option[Config] = {
		val parser = new scopt.OptionParser[Config](toolName) {
			override def showUsageOnError = true
			head(toolName, toolVersion, ": upload file to dropbox")
			help("help") text("prints this usage text")
			
			// one of these two options must be provided to authorize
			opt[Unit]('k', "keychain") action { (x, c) => c.copy(useKeychain = true) } text(s"use Mac OS X keychain to retrieve access token (name = '$accessTokenKeychainServiceName')")
			opt[Unit]('t', "token") action { (x, c) => c.copy(useToken = true) } text("use token for authorization, you have to visit the displayed url to retrieve it") 
			opt[Unit]('o', "overwrite") action { (x, c) => c.copy(overwrite = true) } text("overwrite the file if it already exists at destination (by default the file is renamed)")
			
			arg[File]("<file>") action { (x, c) =>
				c.copy(fileToUpload = x) } text("file to upload")
			
			arg[String]("<destPath>") action { (x, c) =>
				c.copy(destPath = Some(x)) } text("destination path: either a directory (terminated by a slash, e.g. '/Temp/') or a filename ('/Somedir/somefile.ext'). If not specified, this will be '/file'") optional()
				
			checkConfig { c =>
				if (!c.useKeychain && !c.useToken) failure("either keychain or token authorization must be used")
				else if (c.useKeychain && c.useToken) failure("either keychain or token authorization must be specified, not both")
				else success }
		}
		// parser.parse returns Option[Options]
		parser.parse(args, Config())
	}
	
	def authorize(useKeychain: Boolean, appinfoConfFile: String): Either[String, DbxClientV2] = {
		val reqConfig = DbxRequestConfig.newBuilder("ozb-dropbox-upload/1.0").withUserLocale(Locale.getDefault().toString()).build()
		if (useKeychain) {
			KeychainUtils.getKeychainPassword("ozb-dropbox-upload.accessToken") match {
				case None => Left(s"authorization error: could not find $accessTokenKeychainServiceName in keychain !")
				case Some(token) => Right(new DbxClientV2(reqConfig, token))
			}
		}
		else {
			val config = ConfigFactory.parseFile(new java.io.File(appinfoConfFile))
			val appKey = config.getString("appKey")
			val appSecret = config.getString("appSecret")
			
			val appInfo = new DbxAppInfo(rot13(appKey), rot13(appSecret))
			val webAuth = new DbxWebAuth(reqConfig, appInfo)
			val webAuthRequest = DbxWebAuth.newRequestBuilder().withNoRedirect().build()
			val authorizeUrl = webAuth.authorize(webAuthRequest)

			outln("please visit the following url, click allow and copy the auhorization code:")
			outln("  url: %s", authorizeUrl)
			out("enter token (Ctrl-C to cancel): ")
			val code = new java.io.BufferedReader(new java.io.InputStreamReader(System.in)).readLine().trim()
			val authFinish = webAuth.finishFromCode(code)
			Right(new DbxClientV2(reqConfig, authFinish.getAccessToken()))
		}
	}
	
	def upload(client: DbxClientV2, file: File, overwrite: Boolean, destPath: Option[String]) {
		val path = destPath.map { p => 
			val newpath = if (p.endsWith("/")) p + file.getName() else p
			if (newpath.startsWith("/")) newpath 
			else "/" + newpath
		}.getOrElse("/" + file.getName())
		log.info(s"uploading file [$file] to [$path]...")
		val writeMode = if (overwrite) WriteMode.OVERWRITE else WriteMode.ADD
		withInputStream(new java.io.FileInputStream(file)) { in =>
			client.files().uploadBuilder(path).withMode(writeMode).uploadAndFinish(in)
		}
		log.info("done.")
	}
	
	def outln(msg: String, args: Any*) {
		if (args.length == 0) println(msg) 
		else println(msg format (args: _*))
	}

	def out(msg: String, args: Any*) {
		if (args.length == 0) print(msg) 
		else print(msg format (args: _*))
	}

	def rot13(s: String) = s map {
		case c if 'a' <= c.toLower && c.toLower <= 'm' => (c + 13).toChar
		case c if 'n' <= c.toLower && c.toLower <= 'z' => (c - 13).toChar
		case c => c
	}
	
}