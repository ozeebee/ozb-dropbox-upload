# README #

Simple command-line utility to quickly upload file to dropbox.

### Setup

Requires the creation of a Dropbox application.
For convenience, I personnaly generated an access token an created an entry in my Mac OS X Keychain.
The app has a `-k` flag to retrieve it from the Keychain instead of passing by a OAuth 2 steps authorization process. 

The app key and app secret that are used to generate an authorization link should be stored in a config file under `~/.java/ozb-dropbox-upload.appinfo.conf`

ex:

    appKey = 093jkjsfjskdf
    appSecret = klkfjsdjfieiaa

NOTE: the values should be encoded with a [Rot13](http://www.rot13.com) algorithm.

### Building

use sbt as usual. 

I use the sbt-native-packager plugin to generate shell scripts for the tool.

Use the `stage` task to generate them in `target/universal/stage/bin`
